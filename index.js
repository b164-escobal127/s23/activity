
//insertOne Method
db.rooms.insertOne({

    name: "single",

    accomodates: 2,

    price: 1000.00,

    description: "A simple room with all the basic necessities",

    rooms_available: 10,

    isAvailable: false

})    

//insertMany Method

db.rooms.insertMany([

    {

    name: "double",

    accomodates: 3,

    price: 2000,

    description: "A room fit for a small family going on a vacation",

    rooms_available: 5,

    isAvailable: false

    },

    {

    name: "queen",

    accomodates: 4,

    price: 4000,

    description: "A room with a queen sized bed perfect for a simple getaway",

    rooms_available: 15,

    isAvailable: false

    }

])


//find method double

db.rooms.find({

    name: "double"

    })

//updateOne Method
db.rooms.updateOne(

    {name: "queen"},

    {$set: {

            rooms_available: 0

           }

    }

)

//deleteMany method

db.rooms.deleteMany(

    {rooms_available:0}

    )
